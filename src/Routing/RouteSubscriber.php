<?php

namespace Drupal\migrate_qa\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber for Migrate QA.
 *
 * @package Drupal\migrate_qa\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Designate several Migrate QA routes as being admin routes.
    $admin_routes = [
      'entity.migrate_qa_tracker.canonical',
      'entity.migrate_qa_issue.canonical',
    ];
    foreach ($collection->all() as $name => $route) {
      if (in_array($name, $admin_routes)) {
        $route->setOption('_admin_route', TRUE);
      }
    }
  }

}
