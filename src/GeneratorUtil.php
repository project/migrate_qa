<?php

namespace Drupal\migrate_qa;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate_plus\Entity\MigrationGroup;

/**
 * Provides some helper methods for creating updating generator config entities.
 *
 * @package Drupal\migrate_qa
 */
class GeneratorUtil {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Yaml serialization service.
   *
   * @var \Drupal\Component\Serialization\Yaml
   */
  public $serializationYaml;

  /**
   * Module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  public $moduleExtensionList;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public $configFactory;

  /**
   * Util constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   EntityTypeManager service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Yaml $serializationYaml,
    ModuleExtensionList $moduleExtensionList,
    ConfigFactoryInterface $configFactory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->serializationYaml = $serializationYaml;
    $this->moduleExtensionList = $moduleExtensionList;
    $this->configFactory = $configFactory;
  }

  /**
   * Get the contents of a config template file.
   *
   * @param string $template_file
   *   Name of template file to load from config_templates.
   *
   * @return string
   *   Config Yaml.
   */
  public function getConfigTemplate(string $template_file):string {
    $path = $this->moduleExtensionList->getPath('migrate_qa');
    $path .= '/config_templates/' . $template_file;
    return file_get_contents($path);
  }

  /**
   * Convert a config string (yaml) to an array.
   *
   * @param string $config_string
   *   The config string to convert.
   *
   * @return array
   *   The array.
   */
  public function yamlStringToArray(string $config_string):array {
    return $this->serializationYaml->decode($config_string);
  }

  /**
   * Add extra process config to migration config.
   *
   * @param array $migration_config
   *   The migration config.
   * @param string $process_extra
   *   The raw process extra yaml as a string.
   *
   * @return array
   *   Updated migration config array.
   */
  public function addProcessExtra(array $migration_config, string $process_extra):array {
    $process_extra_array = $this->serializationYaml->decode($process_extra);
    if (!empty($process_extra_array)) {
      $migration_config['process'] = array_merge_recursive($migration_config['process'], $process_extra_array);
    }
    return $migration_config;
  }

  /**
   * Add the complete source config.
   *
   * @param array $migration_config
   *   The migration config.
   * @param string $from_migration
   *   The source array to add.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addSource(array $migration_config, string $from_migration) {
    $migration_manager = $this->entityTypeManager->getStorage('migration');
    $migration = $migration_manager->load($from_migration);
    $migration_source = $migration->get('source');
    $migration_group = $migration->get('migration_group');
    if (!empty($migration_group)) {
      $migration_group_object = MigrationGroup::load($migration->migration_group);
      if ($migration_group_object) {
        $migration_group_shared_config = $migration_group_object->get('shared_configuration');
        if (isset($migration_group_shared_config['source'])) {
          // Allow the tracked migration source to override the group source.
          $migration_source = array_merge_recursive($migration_group_shared_config['source'], $migration_source);
        }
      }
    }
    // Prioritize the config of the template over the tracked migration config.
    $migration_config['source'] = array_merge_recursive($migration_source, $migration_config['source']);

    return $migration_config;
  }

  /**
   * @param array $migration_config_array
   *
   * @return \Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveMigrationConfig(array $migration_config_array) {
    $migration_manager = $this->entityTypeManager->getStorage('migration');
    // Delete existing if it exists.
    /** @var \Drupal\migrate_plus\Entity\MigrationInterface $existing_migration_config */
    $existing_migration_config = $migration_manager->load($migration_config_array['id']);
    if ($existing_migration_config) {
      // Save UUID for regenerating migration config.
      $migration_config_uuid = $existing_migration_config->uuid();
      $migration_manager->delete([$existing_migration_config]);
    }
    $migration_config = $migration_manager->create($migration_config_array);
    if (isset($migration_config_uuid)) {
      $migration_config->set('uuid', $migration_config_uuid);
    }
    $migration_config->save();

    // These "enforced" dependencies get copied up a level.
    // If inside the enforced property, they seem to not have an effect.
    // When directly under "dependencies", they do have an effect, but get
    // removed when saving through the fully loaded $migration_config above.
    // So here basic config is used to copy the dependencies.
    $raw_config = $this->configFactory->getEditable('migrate_plus.migration.' . $migration_config_array['id']);
    foreach ($raw_config->get('dependencies.enforced') as $key => $value) {
      $raw_config->set('dependencies.' . $key, $value);
    }
    $raw_config->save();

    return $migration_config;
  }

}
