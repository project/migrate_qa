<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_qa\Entity\ConnectorGenerator;

class TrackerGeneratorForMigrationForm extends GeneratorFormBase {

  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorInterface $config */
    $config = $this->entity;

    $migration_options = $this->entityTypeManager->getStorage('migration')
      ->getQuery()
      // Omit migrate qa migrations to avoid confusion and a compounding mess.
      ->condition('destination.plugin', [
        'entity:migrate_qa_tracker',
        'entity:migrate_qa_connector',
        'entity:migrate_qa_flag',
        'entity:migrate_qa_issue',
      ], 'NOT IN')
      ->accessCheck()
      ->execute();

    // Remove migrations with names that are too long.
    // Maximum table name length is 64 characters.
    // The longest prefix will be `migrate_map_c_tm_` which leaves 47 chars.
    $migration_options = array_filter($migration_options, function($var) {
      return strlen($var) <= 47;
    });

    // Disable migrations that already have associated tracker generators.
    // Only if adding config, because this is not editable after creation.
    if ($config->isNew()) {
      $tracker_generators = $this
        ->entityTypeManager
        ->getStorage('mqa_tracker_generator_migration')
        ->getQuery()
        ->condition('migration', array_keys($migration_options), 'IN')
        ->accessCheck()
        ->execute();
      $migration_options = array_diff($migration_options, $tracker_generators);
    }

    $form['migration'] = [
      '#type' => 'select',
      '#title' => $this->t('Migration'),
      '#default_value' => $config->migration,
      '#options' => $migration_options,
      '#sort_options' => TRUE,
      '#required' => TRUE,
      '#disabled' => !$config->isNew(),
    ];

    $form['source_id_key'] = [
      '#type' => 'textfield',
      '#size' => '20',
      '#title' => $this->t('Source ID Key'),
      '#default_value' => $config->source_id_key ?? '',
      '#required' => TRUE,
      '#description' => $this->t('The key used to identify each source entity. Examples include <em>nid</em>, <em>entity_id</em>, and <em>uuid</em>.'),
    ];

    $entity_types = $this->entityTypeManager->getDefinitions();
    $entity_types_options = [];
    foreach ($entity_types as $name => $definition) {
      if (
        $definition instanceof ContentEntityTypeInterface
        // Filter out content entity types that Migrate QA provides.
        && $definition->getProvider() !== 'migrate_qa'
      ) {
        $entity_types_options[$name] = $definition->getLabel();
      }
    }

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#default_value' => $config->entity_type ?? '',
      '#options' => $entity_types_options,
      '#sort_options' => TRUE,
      '#required' => TRUE,
      '#description' => $this->t('The type of entities created by the selected migration'),
    ];

    $form['process_extra'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Process Extra'),
      '#default_value' => $config->process_extra,
    ];

    return $form;
  }

  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorForMigration $config */
    $config = $this->getEntity();
    if ($config->isNew()) {
      $config->set('id', $config->migration);
    }
    $status = $config->save();

    // Save migration config.
    // TODO Move this to the entity class instead of the form class.
    $yaml_string = $this->generatorUtil->getConfigTemplate('tracker_migration.yml');
    $yaml_string = str_replace('$migration', $config->migration, $yaml_string);
    $yaml_string = str_replace('$id_key', $config->source_id_key, $yaml_string);
    $migration_config_array = $this->generatorUtil->yamlStringToArray($yaml_string);
    $migration_config_array = $this->generatorUtil->addProcessExtra($migration_config_array, $config->process_extra);
    // Add the source migration config.
    $migration_config_array = $this->generatorUtil->addSource($migration_config_array, $config->migration);
    $migration_config = $this->generatorUtil->saveMigrationConfig($migration_config_array);

    if ($status === SAVED_NEW) {
      // Generate connector.
      $connector = ConnectorGenerator::create([
        'id' => $migration_config->id(),
        'tracker_migration' =>  $migration_config->id()
      ]);
      $connector->save();

      $this->messenger()->addMessage($this->t('The %label tracker generator was created.', [
        '%label' => $config->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label tracker generator was updated.', [
        '%label' => $config->label(),
      ]));
    }
  }

  /**
   * Checks for an existing config entity.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    // Use the query factory to build a new robot entity query.
    $storage = $this->entityTypeManager->getStorage('mqa_tracker_generator_migration');
    $query = $storage->getQuery();

    // Query the entity ID to see if its in use.
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->accessCheck()
      ->execute();

    // We don't need to return the ID, only if it exists or not.
    return (bool) $result;
  }

}
