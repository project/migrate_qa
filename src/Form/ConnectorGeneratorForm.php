<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Form;

use Drupal\Core\Form\FormStateInterface;

class ConnectorGeneratorForm extends GeneratorFormBase {

  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorInterface $config */
    $config = $this->entity;

    $query = $this->entityTypeManager->getStorage('migration')->getQuery();
    // Only include migrations that have destination migrate_qa_tracker.
    $query->condition('destination.plugin', 'entity:migrate_qa_tracker');
    $migration_options = $query->accessCheck()->execute();

    // Remove migrations with names that are too long.
    // Maximum table name length is 64 characters.
    // The longest prefix will be `migrate_map_c_tm_` which leaves 47 chars.
    $migration_options = array_filter($migration_options, function($var) {
      return strlen($var) <= 47;
    });

    // Remove tracker migrations that already have associated connector
    // generators.
    // Only if adding config, because this is not editable after creation.
    if ($config->isNew()) {
      $connectors = $this
        ->entityTypeManager
        ->getStorage('mqa_connector_generator')
        ->getQuery()
        ->accessCheck()
        ->execute();
      $migration_options = array_diff($migration_options, $connectors);
    }

    $form['tracker_migration'] = [
      '#type' => 'select',
      '#title' => $this->t('Tracker Migration'),
      '#default_value' => $config->tracker_migration,
      '#options' => $migration_options,
      '#sort_options' => TRUE,
      '#required' => TRUE,
      '#disabled' => !$config->isNew(),
    ];

    $form['process_extra'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Process Extra'),
      '#default_value' => $config->process_extra,
    ];

    return $form;
  }

  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\migrate_qa\Entity\ConnectorGenerator $config */
    $config = $this->getEntity();
    $status = $config->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label connector generator was created.', [
        '%label' => $config->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label connector generator was updated.', [
        '%label' => $config->label(),
      ]));
    }
  }

  /**
   * Checks for an existing config entity.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    // Use the query factory to build a new entity query.
    $storage = $this->entityTypeManager->getStorage('mqa_connector_generator');
    $query = $storage->getQuery();

    // Query the entity ID to see if its in use.
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->accessCheck()
      ->execute();

    return (bool) $result;
  }

}
