<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_qa\Entity\ConnectorGenerator;

class TrackerGeneratorForContentForm extends GeneratorFormBase {

  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorInterface $config */
    $config = $this->entity;

    $entity_types = $this->entityTypeManager->getDefinitions();
    $entity_types_options = [];
    foreach ($entity_types as $name => $definition) {
      if (
        $definition instanceof ContentEntityTypeInterface
        // Filter out content entity types that Migrate QA provides.
        && $definition->getProvider() !== 'migrate_qa'
        // (Temporary) Omit entity types with underscores in name.
        && !str_contains($name, '_')
      ) {
        $entity_types_options[$name] = $definition->getLabel();
      }
    }

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#default_value' => $config->entity_type,
      '#options' => $entity_types_options,
      '#sort_options' => TRUE,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::bundlesField',
        'wrapper' => 'bundle-container',
      ],
      '#disabled' => !$config->isNew(),
    ];

    $form['bundle_container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'bundle-container'],
    ];
    $selected_entity_type = $form_state->getValue('entity_type') ?? $config->entity_type;
    $bundles_options = [];
    if (!empty($selected_entity_type)) {
      $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($selected_entity_type);
      foreach ($bundle_info as $id => $value) {
        // Skip bundles with names that are too long when combined with the
        // entity type name.
        // Maximum table name length is 64 characters.
        // The longest prefix will be `migrate_map_c_tc_` which leaves 47 chars.
        // Also, an underscore is used to separate entity type and bundle in the
        // migration name.
        if (strlen($selected_entity_type) + strlen('_') + strlen($id) > 47) {
          continue;
        }

        // Skip if there is a tracker generator for this entity type / bundle.
        // Only if adding config, because this is not editable after creation.
        if ($config->isNew()) {
          $tracker_generator = $this
            ->entityTypeManager
            ->getStorage('mqa_tracker_generator_content')
            ->getQuery()
            ->condition('entity_type', $selected_entity_type)
            ->condition('bundle', $id)
            ->accessCheck()
            ->execute();
          if ($tracker_generator) {
            continue;
          }
        }
        $bundles_options[$id] = $value['label'];
      }
    }
    $form['bundle_container']['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#default_value' => $config->bundle,
      '#options' => $bundles_options,
      '#required' => TRUE,
      '#disabled' => !$config->isNew(),
    ];
    // Disable if there is no active entity type.
    if (!$selected_entity_type) {
      $form['bundle_container']['bundle']['#disabled'] = TRUE;
    }
    // Disable if no options are available.
    if (empty($bundles_options)) {
      $form['bundle_container']['bundle']['#disabled'] = TRUE;
    }

    $form['process_extra'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Process Extra'),
      '#default_value' => $config->process_extra,
    ];

    return $form;
  }

  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorForContent $config */
    $config = $this->getEntity();
    if ($config->isNew()) {
      $config->set('id', $config->entity_type . '.' . $config->bundle);
    }
    $status = $config->save();

    // Save migration config.
    // TODO move this to the entity class instead of the form class.
    $entity_keys = $this->entityTypeManager->getDefinition($config->entity_type)->get('entity_keys');
    $bundle_entity_type = $this->entityTypeManager->getDefinition($config->entity_type)->get('bundle_entity_type');
    $entity_type_info = $this->entityTypeManager->getDefinition($bundle_entity_type);
    $config_prefix = $entity_type_info->get('config_prefix');
    $yaml_string = $this->generatorUtil->getConfigTemplate('tracker_content.yml');
    $yaml_string = str_replace('$entity_type', $config->entity_type, $yaml_string);
    $yaml_string = str_replace('$config_prefix', $config_prefix, $yaml_string);
    $yaml_string = str_replace('$bundle', $config->bundle, $yaml_string);
    $yaml_string = str_replace('$id_key', $entity_keys['id'], $yaml_string);
    $migration_config_array = $this->generatorUtil->yamlStringToArray($yaml_string);
    $migration_config_array = $this->generatorUtil->addProcessExtra($migration_config_array, $config->process_extra);
    $migration_config = $this->generatorUtil->saveMigrationConfig($migration_config_array);

    if ($status === SAVED_NEW) {
      // Generate connector.
      $connector = ConnectorGenerator::create([
        'id' => $migration_config->id(),
        'tracker_migration' =>  $migration_config->id()
      ]);
      $connector->save();

      $this->messenger()->addMessage($this->t('The %label tracker generator was created.', [
        '%label' => $config->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label tracker generator was updated.', [
        '%label' => $config->label(),
      ]));
    }
  }

  /**
   * Checks for an existing config entity.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    // Use the query factory to build a new robot entity query.
    $storage = $this->entityTypeManager->getStorage('mqa_tracker_generator_content');
    $query = $storage->getQuery();

    // Query the entity ID to see if its in use.
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->accessCheck()
      ->execute();

    // We don't need to return the ID, only if it exists or not.
    return (bool) $result;
  }

  /**
   * Ajax callback to update the available bundles.
   *
   * @param array $form
   *   Structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   *
   * @return array
   *   Render array portion that will replace the bundles field.
   */
  public function bundlesField(array $form, FormStateInterface $form_state) {
    return $form['bundle_container'];
  }

}
