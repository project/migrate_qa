<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\migrate_qa\GeneratorUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class GeneratorFormBase extends EntityForm {

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  public $entityTypeBundleInfo;

  /**
   * Generator utility commands.
   *
   * @var \Drupal\migrate_qa\GeneratorUtil
   */
  public $generatorUtil;

  /**
   * Constructs a TrackerGeneratorForContentForm object.
   */
  public function __construct(
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    GeneratorUtil $generatorUtil
  ) {
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->generatorUtil = $generatorUtil;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('migrate_qa.generator_util')
    );
  }

}
