<?php

namespace Drupal\migrate_qa\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of Issue entities.
 */
class IssueListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['summary'] = $this->t('Summary');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\migrate_qa\Entity\TrackerInterface $entity */
    $row['id'] = $entity->id();
    $row['summary'] = $entity->toLink()->toString();

    return $row + parent::buildRow($entity);
  }

}
