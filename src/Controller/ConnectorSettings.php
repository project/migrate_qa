<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

class ConnectorSettings extends ControllerBase {

  /**
   * Contents of the main settings page for connectors.
   *
   * @return array
   *   Renderable array.
   */
  public function overview() {

    $render['connectors'] = [
      '#markup' => '<p>' . Link::createFromRoute(
        'Manage connector generators',
        'entity.mqa_connector_generator.collection'
      )->toString() . '</p>'
    ];

    return $render;
  }
}
