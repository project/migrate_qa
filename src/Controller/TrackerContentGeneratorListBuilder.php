<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class to build a listing of TrackerContentGenerator entities.
 */
class TrackerContentGeneratorListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'migrate_qa';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['entity_type'] = $this->t('Entity Type');
    $header['bundle'] = $this->t('Bundle');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorForContent $entity */
    $row['entity_type'] = $entity->entity_type;
    $row['bundle'] = $entity->bundle;
    return $row + parent::buildRow($entity);
  }

}
