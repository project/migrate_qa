<?php

namespace Drupal\migrate_qa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\migrate_qa\Util;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Tracker Edit routes.
 */
class TrackerEdit extends ControllerBase {

  /**
   * The Migrate QA Util service.
   *
   * @var \Drupal\migrate_qa\Util
   */
  protected $migrateQaUtil;

  /**
   * The entity form builder service.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * TrackerEdit constructor.
   *
   * @param \Drupal\migrate_qa\Util $migrate_qa_util
   *   The Migrate QA Util service.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The form builder service.
   */
  public function __construct(
    Util $migrate_qa_util,
    EntityFormBuilderInterface $entity_form_builder
  ) {
    $this->migrateQaUtil = $migrate_qa_util;
    $this->entityFormBuilder = $entity_form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('migrate_qa.util'),
      $container->get('entity.form_builder')
    );
  }

  /**
   * Display an edit form for the associated tracker.
   *
   * @return array
   *   Render array including the edit form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function editForm() {
    $entity = $this->migrateQaUtil->getCurrentEntity();
    $tracker = $this->migrateQaUtil->getTracker($entity);
    if ($tracker === NULL) {
      return ['#markup' => $this->t('No note found (Migrate QA Tracker entity)')];
    }
    return $this->entityFormBuilder->getForm($tracker, 'default');
  }

  /**
   * Create a title for tracker edit page.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node to be used for creating the label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function nodeTitle(Node $node) {
    $label = $node->label();
    return $this->t('Edit note for %label', ['%label' => $label]);
  }

}
