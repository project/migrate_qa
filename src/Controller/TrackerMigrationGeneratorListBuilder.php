<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class to build a listing of TrackerMigrationGenerator entities.
 */
class TrackerMigrationGeneratorListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'migrate_qa';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['migration'] = $this->t('Migration');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorForMigration $entity */
    $row['migration'] = $entity->migration;
    return $row + parent::buildRow($entity);
  }

}
