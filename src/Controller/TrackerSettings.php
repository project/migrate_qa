<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

class TrackerSettings extends ControllerBase {

  /**
   * Contents of the main settings page for trackers.
   *
   * @return array
   *   Renderable array.
   */
  public function overview() {

    $render['generators_for_content'] = [
      '#markup' => '<p>' . Link::createFromRoute(
        'Manage tracker generators for content',
        'entity.mqa_tracker_generator_content.collection'
      )->toString() . '</p>'
    ];

    $render['generators_for_migration'] = [
      '#markup' => '<p>' . Link::createFromRoute(
          'Manage tracker generators for migration',
          'entity.mqa_tracker_generator_migration.collection'
        )->toString() . '</p>'
    ];

    return $render;
  }
}
