<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class to build a listing of ConnectorGenerator entities.
 */
class ConnectorGeneratorListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['tracker_migration'] = $this->t('Tracker Migration');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\migrate_qa\Entity\TrackerGeneratorForMigration $entity */
    $row['id'] = $entity->id();
    $row['tracker_migration'] = $entity->tracker_migration;
    return $row + parent::buildRow($entity);
  }

}
