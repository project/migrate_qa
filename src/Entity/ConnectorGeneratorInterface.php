<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Migrate QA Tracker Generator Interface.
 */
interface ConnectorGeneratorInterface extends ConfigEntityInterface {

}
