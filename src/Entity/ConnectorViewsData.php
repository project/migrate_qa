<?php

namespace Drupal\migrate_qa\Entity;

/**
 * Provides Views data for Migrate QA Connector entities.
 */
class ConnectorViewsData extends EntityReferenceViewsData {

}
