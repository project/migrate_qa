<?php

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\TypedData\TranslationStatusInterface;

/**
 * Provides an Interface for the Issue Entity Type.
 */
interface IssueInterface extends EntityChangedInterface, RevisionLogInterface, ContentEntityInterface, TranslationStatusInterface {

}
