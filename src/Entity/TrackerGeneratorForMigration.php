<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Migrate QA Tracker Generator for Migration.
 *
 * @ConfigEntityType(
 *   id = "mqa_tracker_generator_migration",
 *   label = @Translation("Tracker Generator for Migration"),
 *   handlers = {
 *     "list_builder" = "Drupal\migrate_qa\Controller\TrackerMigrationGeneratorListBuilder",
 *     "form" = {
 *       "default" = "Drupal\migrate_qa\Form\TrackerGeneratorForMigrationForm",
 *       "add" = "Drupal\migrate_qa\Form\TrackerGeneratorForMigrationForm",
 *       "edit" = "Drupal\migrate_qa\Form\TrackerGeneratorForMigrationForm",
 *       "delete" = "Drupal\migrate_qa\Form\TrackerGeneratorForMigrationDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "tracker_generator_migration",
 *   admin_permission = "administer migrate_qa_tracker generator",
 *   entity_keys = {
 *     "id" = "id",
 *     "migration" = "migration",
 *     "source_id_key" = "source_id_key",
 *     "entity_type" = "entity_type",
 *     "process_extra" = "process_extra"
 *   },
 *   config_export = {
 *     "id",
 *     "migration",
 *     "source_id_key",
 *     "entity_type",
 *     "process_extra"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/migrate-qa/tracker/settings/generators-migration/add",
 *     "edit-form" = "/admin/structure/migrate-qa/tracker/settings/generators-migration/{mqa_tracker_generator_migration}/edit",
 *     "delete-form" = "/admin/structure/migrate-qa/tracker/settings/generators-migration/{mqa_tracker_generator_migration}/delete",
 *     "collection" = "/admin/structure/migrate-qa/tracker/settings/generators-migration",
 *   }
 * )
 */
class TrackerGeneratorForMigration extends ConfigEntityBase implements TrackerGeneratorInterface {

  /**
   * Migration that this config relates to.
   *
   * @var string
   */
  public $migration;

  /**
   * Extra Yaml to include in the migration process section.
   *
   * @var string
   */
  public $process_extra;

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Fall back to the entity ID if the default label is empty.
    $label = parent::label();
    if (!$label) {
      $label = $this->id();
    }
    return $label;
  }

}
