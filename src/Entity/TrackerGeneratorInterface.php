<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Migrate QA Tracker Generator Interface.
 */
interface TrackerGeneratorInterface extends ConfigEntityInterface {

//  public function getEntityType();
//
//  public function getBundle();
//
//  public function getProcessExtra();
}
