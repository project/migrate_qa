<?php

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides Views data for Migrate QA Flag entities.
 */
class FlagViewsData extends EntityReferenceViewsData {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['migrate_qa_flag']['details_count'] = [
      'title' => $this->t('Details Count'),
      'field' => [
        'id' => 'migrate_qa_flag_details_count',
      ],
    ];

    return $data;
  }

}
