<?php

declare(strict_types=1);

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Migrate QA Tracker Generator for Content.
 *
 * @ConfigEntityType(
 *   id = "mqa_tracker_generator_content",
 *   label = @Translation("Tracker Generator for Content"),
 *   handlers = {
 *     "list_builder" = "Drupal\migrate_qa\Controller\TrackerContentGeneratorListBuilder",
 *     "form" = {
 *       "default" = "Drupal\migrate_qa\Form\TrackerGeneratorForContentForm",
 *       "add" = "Drupal\migrate_qa\Form\TrackerGeneratorForContentForm",
 *       "edit" = "Drupal\migrate_qa\Form\TrackerGeneratorForContentForm",
 *       "delete" = "Drupal\migrate_qa\Form\TrackerGeneratorForContentDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "tracker_generator_content",
 *   admin_permission = "administer migrate_qa_tracker generator",
 *   entity_keys = {
 *     "id" = "id",
 *     "entity_type" = "entity_type",
 *     "bundle" = "bundle",
 *     "process_extra" = "process_extra"
 *   },
 *   config_export = {
 *     "id",
 *     "entity_type",
 *     "bundle",
 *     "process_extra"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/migrate-qa/tracker/settings/generators-content/add",
 *     "edit-form" = "/admin/structure/migrate-qa/tracker/settings/generators-content/{mqa_tracker_generator_content}/edit",
 *     "delete-form" = "/admin/structure/migrate-qa/tracker/settings/generators-content/{mqa_tracker_generator_content}/delete",
 *     "collection" = "/admin/structure/migrate-qa/tracker/settings/generators-content",
 *   }
 * )
 */
class TrackerGeneratorForContent extends ConfigEntityBase implements TrackerGeneratorInterface {

  /**
   * Entity type that this config relates to.
   *
   * @var string
   */
  public $entity_type;

  /**
   * Bundle that this config relatest to.
   *
   * @var string.
   */
  public $bundle;

  /**
   * Extra Yaml to include in the migration process section.
   *
   * @var string
   */
  public $process_extra;

  /**
   * Tracker constructor.
   *
   * Override default constructor to set some custom properties.
   *
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);
    $this->time = \Drupal::service('datetime.time');
    $this->currentUser = \Drupal::currentUser();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Fall back to the entity ID if the default label is empty.
    $label = parent::label();
    if (!$label) {
      $label = $this->id();
    }
    return $label;
  }

}
