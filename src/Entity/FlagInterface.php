<?php

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an Interface for the Flag Content Type.
 */
interface FlagInterface extends ContentEntityInterface {

}
