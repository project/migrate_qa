<?php

namespace Drupal\migrate_qa\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an Interface to the Connector Entity Type.
 */
interface ConnectorInterface extends ContentEntityInterface {

}
