<?php

namespace Drupal\migrate_qa\Plugin\migrate\source;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Source plugin for Sql content, manually created nodes.
 *
 * @MigrateSource(
 *   id = "mqa_content"
 * )
 */
class MqaContent extends SqlBase {

  /**
   * The entity type to be queried.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The bundle to be queried.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The definition of the requested entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityTypeDefinition;

  /**
   * @throws \Drupal\migrate\MigrateException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityTypeManagerInterface $entityTypeManager) {
    if (empty($configuration['entity_type'])) {
      throw new MigrateException('The entity_type config is required in the mqa_content source');
    }
    if (empty($configuration['bundle'])) {
      throw new MigrateException('The bundle config is required in the mqa_content source');
    }
    $this->entityType = $configuration['entity_type'];
    $this->bundle = $configuration['bundle'];
    $this->entityTypeDefinition = $entityTypeManager->getDefinition($this->entityType);
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // TODO Omit nodes that were imported.
    // TODO Or otherwise protect against importing duplicates.
    $entity_keys = $this->entityTypeDefinition->get('entity_keys');
    $base_table = $this->entityTypeDefinition->get('base_table');
    $base_table_alias = 'b';

    $query = $this->select($base_table, $base_table_alias);
    $query->addField($base_table_alias, $entity_keys['id']);
    $query->condition($entity_keys['bundle'], $this->bundle);
    $query->orderBy($base_table_alias . '.' . $entity_keys['id']);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $entity_keys = $this->entityTypeDefinition->get('entity_keys');
    return [
      $entity_keys['id'] => [
        'type' => 'integer',
        'alias' => 'b',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $entity_keys = $this->entityTypeDefinition->get('entity_keys');
    return [
      $entity_keys['id'] => $this->t('Entity ID'),
    ];
  }

}
