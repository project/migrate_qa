<?php

namespace Drupal\migrate_qa\Plugin\migrate\process;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Prepare a simple string for a formatted text field, with a text format.
 *
 * The resulting value is an array of arrays.
 * The inner array has value and format keys.
 * For use before a sub_process, so that the sub-array is associative.
 *
 * Available configuration keys:
 * - values: Array of strings to prepare for a text field.
 * - format: Text format for the field.
 *
 * Example:
 *
 * @code
 * process:
 *   field_example_body:
 *     plugin: formatted_text_prepare
 *     source: foo
 *     values:
 *       - 'Example One'
 *       - 'Example Two'
 *       - 'Example Three'
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "formatted_text_prepare"
 * )
 */
class FormattedTextPrepare extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Make $value an array if it's not already.
    if (!is_array($value)) {
      $value = [$value];
    }
    if (!isset($this->configuration['format'])) {
      throw new MigrateException('The formatted_text_prepare plugin is missing format configuration.');
    }

    $format = $this->configuration['format'];
    $values = [];
    foreach ($value as $item) {
      $values[] = [
        'value' => $item,
        'format' => $format,
      ];
    }

    return $values;
  }

}
