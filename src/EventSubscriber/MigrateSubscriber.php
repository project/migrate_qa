<?php

namespace Drupal\migrate_qa\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\State;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Migrate QA event subscriber.
 */
class MigrateSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The state manager.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Constructs a PostRowSave subscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, State $state) {
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
  }

  /**
   * Pre row save event handler.
   *
   * @param \Drupal\migrate\Event\MigratePreRowSaveEvent $event
   *   Pre row save event.
   */
  public function onMigratePreRowSave(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    $row->setDestinationProperty('created_by_migration', $event->getMigration()->id());
    $row->setDestinationProperty('id_at_source', $row->getSourceIdValues());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      MigrateEvents::PRE_ROW_SAVE => ['onMigratePreRowSave'],
    ];
  }

}
