<?php

namespace Drupal\migrate_qa\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Migrate QA event subscriber.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * Config save handler.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config CRUD event.
   */
  public function onConfigSave(ConfigCrudEvent $event) {
    // Add missing dependencies to connector generators.
    $config = $event->getConfig();
    if (str_starts_with($config->getName(), 'migrate_qa.connector_generator.') ) {
      $dependencies = $config->get('dependencies');
      if (
        !array_key_exists('config', $dependencies)
        && !empty($dependencies['enforced']['config'])
      ) {
        $dependencies['config'] = $dependencies['enforced']['config'];
        $config->set('dependencies', $dependencies);
        $config->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ConfigEvents::SAVE => ['onConfigSave'],
    ];
  }

}
